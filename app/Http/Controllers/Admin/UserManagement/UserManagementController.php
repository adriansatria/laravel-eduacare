<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;

class UserManagementController extends Controller
{
    public function getData(Request $request)
    {
        if($request->ajax()) {
            $data = User::latest()->where('level', '!=', 1)->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('level', function($data) {
                    if($data->level == 2) {
                        return '<span class="badge badge-primary">USER</span>';
                    } else {
                        return '<span class="badge badge-warning">another</span>';
                    }
                })->rawColumns(['level'])
                // ->addColumn('action', function($row) {
                //     $btn = '<a href="'.route('admin.user.edit', $row->id).'" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>';
                //     $btn .= '&nbsp;&nbsp;';
                //     $btn .= '<a href="'.route('admin.user.delete', $row->id).'" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>';
                //     return $btn;
                // })
                // ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.roleuser.index');
    }
}
