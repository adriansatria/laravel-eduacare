<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class HomeController extends Controller
{
    
    public function index()
    {
        $user = User::find(1);
        return view('home.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function profile() {
        return view('users.index');
    }

    public function updateProfile(Request $request, $id) {
        $user = User::find($id);
        $file = $request->photo;
        if($file) {
            $fileName = $file->getClientOriginalName();
            $path = public_path('photos/'. Auth::user()->id . '/' . Auth::user()->username);
            $file->move($path, $fileName);
            $user->photo = $fileName;
        } else {
            $user->photo = $user->photo;
        }
        // $fileName = $file->getClientOriginalName();
        $user->username = $request->username;
        $user->email = $request->email;
        if($request->password != null) {
            $user->password = bcrypt($request->password);
        } else {
            $user->password = $user->password;
        }
        $user->save();

        return redirect('users/profile')->with('success', 'Profil berhasil diubah!');
    }

    public function deleteAccount() {
        $user = User::find(Auth::user()->id);
        $user->delete();
        return redirect('/')->with('success', 'Akun berhasil dihapus!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
