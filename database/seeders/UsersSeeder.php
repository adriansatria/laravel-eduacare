<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $users = [
            [
                'email' => 'admin@gmail.com',
                'username' => 'admin',
                'password' => bcrypt('admin123'),
                'level' => '1',
            ],
            [
                'email' => 'jhondoe@gmail.com',
                'username' => 'Jhon Doe',
                'password' => bcrypt('jhondon123'),
                'level' => '2',
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
