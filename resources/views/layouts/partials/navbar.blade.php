<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel Educare</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('home.index') }}"><img src="{{ asset('assets/img/bootstrap-logo.svg') }}" alt="" width="40" height="34"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll"
                aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarScroll">
                <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('home.index') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                </ul>
                <form class="d-flex my-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" aria-label="Search"
                            aria-describedby="button-addon2">
                        <button class="btn btn-primary" type="button" id="button-addon2">Search</button>
                    </div>
                </form>
                @guest
                <form class="m-2">
                    <a href="{{ route('auth.login') }}" class="btn btn-warning" type="submit">Login</a>
                </form>
                @endguest
                @auth
    
                <div class="btn-group m-2">
                    <img src="{{ Auth::user()->photo == null ? asset('assets/img/avatar/avatar6_big@2x.png') : asset('photos/'. Auth::user()->id .'/'. Auth::user()->username .'/'. Auth::user()->photo) }}" width="40" height="40" alt="" class="rounded-circle" type="button" id="dropdownMenuClickableOutside"
                    data-bs-toggle="dropdown" data-bs-auto-close="inside" aria-expanded="false">
                    <ul class="dropdown-menu dropdown-menu-end mt-2" aria-labelledby="dropdownMenuClickableOutside">
                        <li class="text-start px-3">{{ Auth::user()->username }}
                            @php
                            $user = Auth::user();
                            $getLevel = Auth::user()->level;
                            $valid = $user->getRole($getLevel);
                            @endphp
                            @if($valid == 'User')
                                <span class="badge bg-primary">user</span>
                            @endif
                        </li>
                        <hr>
                        @if (Auth::user()->level != 1)
                        <li><a class="dropdown-item" href="{{ route('home.profile') }}">
                            <button class="btn">
                                <i class="far fa-user"></i> Profile
                            </button>
                        </a></li>
                        @endif
                        <li><a class="dropdown-item">
                                <form method="POST" action="{{ route('auth.logout') }}">
                                    @csrf
                                    <button type="submit" class="btn">
                                        <i class="bi bi-box-arrow-right"></i> {{ __('Logout') }}
                                    </button>
                                </form>
                            </a>
                        </li>
                    </ul>
                </div>
                @endauth
            </div>
        </div>
    </nav>
    
    <script>
        var dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'))
        var dropdownList = dropdownElementList.map(function (dropdownToggleEl) {
            return new bootstrap.Dropdown(dropdownToggleEl)
        })
    
    </script>
</body>
</html>
