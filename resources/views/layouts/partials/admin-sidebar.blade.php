<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="../../index3.html" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-2">
                    <span>{{ Auth::user()->username }}</span>
                    <div class="btn-group m-2">
                        <img src="{{ Auth::user()->photo == null ? asset('assets/img/avatar/avatar6_big@2x.png') : asset('photos/'. Auth::user()->id .'/'. Auth::user()->username .'/'. Auth::user()->photo) }}"
                            width="40" height="40" alt="" class="rounded-circle" type="button"
                            id="dropdownMenuClickableOutside" data-bs-toggle="dropdown" data-bs-auto-close="inside"
                            aria-expanded="false">
                        <ul class="dropdown-menu dropdown-menu-end mt-2" aria-labelledby="dropdownMenuClickableOutside">
                            <li><a class="dropdown-item">
                                    <form method="POST" action="{{ route('auth.logout') }}">
                                        @csrf
                                        <button type="submit" class="btn">
                                            <i class="bi bi-box-arrow-right"></i> {{ __('Logout') }}
                                        </button>
                                    </form>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="{{ asset('assets/img/bootstrap-logo.svg') }}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3">
                <span class="brand-text font-weight-light">AdminLTE 3</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-tab nav-sidebar flex-column" id="myTab" role="tablist" data-widget="treeview"
                        role="menu" data-accordion="false">
                        <li class="nav-item" role="presentation">
                            <a href="#user-management" data-bs-toggle="pill"
                                role="tab" aria-controls="user-management" aria-selected="true" class="nav-link"><i
                                    class="bi bi-table"></i>
                                <p>
                                    User Management
                                </p>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a href="#test" data-bs-toggle="pill" role="tab" aria-controls="test"
                                aria-selected="false" class="nav-link"><i class="bi bi-table"></i>
                                <p>
                                    User
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="container-fluid tab-content">
                <div class="tab-pane active" id="user-management" role="tabpanel" aria-labelledby="user-management-tab">
                    <div class="row">
                        <div class="col-12">
                            <!-- Default box -->
                            @include('admin.roleuser.index')
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="test" role="tabpanel" aria-labelledby="test-tab">
                    <div class="row">
                        <div class="col-12">
                            <!-- Default box -->
                            <p>Hello Adrian</p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.2.0
            </div>
            <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    @push('scripts')
    <!-- jQuery -->
    {{-- <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script> --}}
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    @endpush
</body>

</html>
