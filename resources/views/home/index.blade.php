@extends('layouts.app-master')

@section('content')
<div class="bg-light p-4 rounded">
    {{-- Jika login masuk ke sini --}}
    @auth
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Home</li>
            </ol>
        </nav>
        <span>Welcome {{ Auth::user()->username }}</span>
    </div>
    @endauth

    {{-- Jika guest masuk ke sini --}}
    @guest
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
    <div class="container">
        <h3>Dashboard</h3>
        <p class="lead">Selamat Datang!!</p>
    </div>
    @endguest
</div>

@endsection
