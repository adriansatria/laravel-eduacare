{{-- @extends('layouts.auth-master')

@section('content')
<!-- Section: Design Block -->
<section class="">
    <!-- Jumbotron -->
    <div class="px-4 py-5 px-md-5 text-center text-lg-start" style="background-color: hsl(0, 0%, 96%);">
        <div class="container">
            <div class="row gx-lg-5 align-items-center">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <h1 class="my-5 display-4 fw-bold ls-tight">
                        Silahkan login untuk <br />
                        <span class="text-primary">melanjutkan</span>
                    </h1>
                    <p style="color: hsl(217, 10%, 50.8%)">
                        Pastikan Anda telah terdaftar sebagai member. Nikmati semua fitur yang sudah kami sediakan demi
                        kemudahan pengguna.
                    </p>
                </div>

                <div class="col-lg-6 mb-5 mb-lg-0">
                    <div class="card">
                        <div class="card-body py-5 px-md-5">
                            <form class="needs-validation" novalidate>
                                <div>
                                    <img src="assets/img/bootstrap-logo.svg" alt="" width="40" height="34">
                                </div>
                                <div class="text-center">
                                    <h3 class="mb-3 fw-light">Login</h3>
                                </div>
                                <!-- Email input -->
                                <div class="mb-3 mt-3">
                                    <label for="uname" class="form-label">Email or Username</label>
                                    <input type="text" class="form-control" id="uname"
                                        placeholder="Enter Email atau Username" name="uname" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Email atau Username harus diisi!</div>
                                </div>

                                <!-- Password input -->
                                <div class="mb-3 mt-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="uname" placeholder=""
                                        name="password" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Password harus diisi!</div>
                                </div>

                                <div class="row">
                                    <div class="col-md">
                                        <span>Belum punya akun? <a href="{!! route('auth.register') !!}">daftar
                                                baru</a></span>
                                    </div>
                                </div>

                                <!-- Submit button -->
                                <button type="submit" class="float-end btn btn-primary btn-block mb-4">
                                    Sign In
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->
</section>
<!-- Section: Design Block -->
@endsection --}}
