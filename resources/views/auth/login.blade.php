@extends('layouts.auth-master')

@section('content')
<form method="post" action="{{ route('login.perform') }}">
    @csrf
    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
    <img class="mb-4" src="{!! url('assets/img/bootstrap-logo.svg') !!}" alt="" width="72" height="57">

    <h1 class="h3 mb-3 fw-normal">Login</h1>

    @include('layouts.partials.messages')
    @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

    <div class="form-group form-floating mb-3">
        <input type="text" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" value="{{ old('email') }}" placeholder="email"
            required="required" autofocus>
        <label for="floatingName">Email</label>
        @if ($errors->has('email'))
        <span class="text-danger text-left">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="form-group form-floating mb-3">
        <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Password"
            required="required">
        <label for="floatingPassword">Password</label>
        @if ($errors->has('password'))
        <span class="text-danger text-left">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="row mb-3">
        <div class="col-sm">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    Remember me
                </label>
            </div>
        </div>
        <div class="col-sm text-end">
            <a href="{{ route('register.perform') }}">Daftar baru</a>
        </div>
    </div>

    {{-- <div class="form-group mb-3">
        <label for="remember">Remember me</label>
        <input type="checkbox" name="remember" value="1">
    </div> --}}

    <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>

    {{-- @include('auth.partials.copy') --}}
</form>
@endsection
