{{-- @extends('layouts.app-master')

@section('content')

<!-- Section: Design Block -->
<section class="">
    <!-- Jumbotron -->
    <div class="px-4 py-5 px-md-5 text-center text-lg-start" style="background-color: hsl(0, 0%, 96%);">
        <div class="container">
            <div class="row gx-lg-5 align-items-center">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <h1 class="my-5 display-4 fw-bold ls-tight">
                        Silahkan Registrasi untuk <br />
                        <span class="text-primary">melanjutkan</span>
                    </h1>
                    <p style="color: hsl(217, 10%, 50.8%)">
                        Silahkan lakukan Registrasi bila Anda belum mempunyai akun, dan pastikan data telah terisi
                        dengan benar.
                    </p>
                </div>

                <div class="col-lg-6 mb-5 mb-lg-0">
                    <div class="card">
                        <div class="card-body py-5 px-md-5">
                            <form class="needs-validation" novalidate>
                                <div>
                                    <img src="assets/img/bootstrap-logo.svg" alt="" width="40" height="34">
                                </div>
                                <div class="text-center">
                                    <h3 class="mb-3 fw-light">Daftar Baru</h3>
                                </div>

                                <!-- Nama input -->
                                <div class="mb-3 mt-3">
                                    <label for="name" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="name"
                                        placeholder="" name="name" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Nama harus diisi!</div>
                                </div>

                                <!-- Email input -->
                                <div class="mb-3 mt-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="text" class="form-control" id="email"
                                        placeholder="" name="email" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Email harus diisi!</div>
                                </div>


                                <!-- Email input -->
                                <div class="mb-3 mt-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password"
                                        placeholder="" name="password" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Password harus diisi! Min. 8 karakter</div>
                                </div>


                                <!-- Password input -->
                                <div class="mb-3 mt-3">
                                    <label for="cpassword" class="form-label">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="cpassword"
                                        placeholder="" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Password harus diisi! Min. 8 karakter</div>
                                </div>


                                <!-- Submit button -->
                                <button type="submit" class="float-end btn btn-primary btn-block">
                                    Sign Up
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->
</section>
<!-- Section: Design Block -->

@endsection --}}
