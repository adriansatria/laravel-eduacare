@extends('layouts.app-master')

@section('content')
<div class="container mt-5">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Profile</h5>
            <form action="{{ url('/users/profile') }}/{{ Auth::user()->id }}" enctype="multipart/form-data"
                method="POST">
                @method('patch')
                @csrf
                <img class="rounded-circle my-2"
                    src="{{ Auth::user()->photo == null ? asset('assets/img/avatar/avatar6_big@2x.png') : asset('photos/'. Auth::user()->id .'/'. Auth::user()->username .'/'. Auth::user()->photo) }}"
                    width="200" height="200" id="preview-image">
                <div class="mb-3">
                    <label for="formFile" class="form-label">Upload Photo</label>
                    <input class="form-control" type="file" accept="image/*" name="photo" id="formFile"
                        {{ old('photo') }}>
                </div>
                <div class="form-group">
                    <label for="username">username</label>
                    <input type="text" class="form-control" id="username" name="username"
                        value="{{ Auth::user()->username }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}"
                        readonly>
                </div>
                <div class="form-check my-2 form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked">
                    <label class="form-check-label" for="flexSwitchCheckChecked">Enable Change Password</label>
                </div>
                <div class="password">

                </div>
                <div class="float-end mt-2">
                    <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal"
                        data-bs-target="#myModal">Delete this Account</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- MODAL --}}
<div class="modal" id="myModal" tabindex="-1">
    <form action="{{ url('users/profile/'. Auth::user()->id) }}" method="POST">
        @method('delete')
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="bi bi-exclamation-triangle"></i> Peringatan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin menghapus Akun?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var checkbox = document.querySelector('#flexSwitchCheckChecked');
        var password = document.querySelector('.password');
        checkbox.addEventListener('change', function () {
            if (this.checked) {
                password.innerHTML = `
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="password">Confirm Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                `;
            } else {
                password.innerHTML = '';
            }
        });

        document.getElementById('formFile').addEventListener('change', function () {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                document.getElementById('preview-image').src = e.target.result;
            };
            reader.readAsDataURL(file);
        });
    });

</script>
