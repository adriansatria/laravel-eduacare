<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\Admin\UserManagement\UserManagementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::group(['middleware' => ['guest']], function () {
    // Login Routes...
    Route::get('/login', [LoginController::class, 'index'])->name('auth.login');
    Route::post('/login', [LoginController::class, 'login'])->name('login.perform');

    // Registration Routes...
    Route::get('/register', [RegisterController::class, 'index'])->name('auth.register');
    Route::post('/register', [RegisterController::class, 'register'])->name('register.perform');
});

Route::group(['prefix' => 'users', 'middleware' => ['auth', 'isUsers']], function () {
    Route::get('/profile', [HomeController::class, 'profile'])->name('home.profile');
    Route::patch('/profile/{id}', [HomeController::class, 'updateProfile'])->name('home.updateProfile');
    Route::delete('/profile/{id}', [HomeController::class, 'deleteAccount'])->name('home.deleteAccount');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin']], function() {
    // Route::get('/profile', [HomeController::class, 'profile'])->name('home.profile');
    // Route::patch('/profile/{id}', [HomeController::class, 'updateProfile'])->name('home.updateProfile');
    Route::get('/', function() {
        return view('layouts.admin-master');
    });
    Route::get('/getData', [UserManagementController::class, 'getData'])->name('admin.user.getData');
});

Route::post('/logout', [LoginController::class, 'logout'])->name('auth.logout');
